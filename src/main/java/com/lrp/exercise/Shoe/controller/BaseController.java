package com.lrp.exercise.Shoe.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RequestMapping(path="/api")
public class BaseController {

    public final static String BASE_API_V1 = "/api/v1";

    protected ResponseEntity<?> getCreatedHttpResponse(Object entityId) {
        // Set the location header for the newly created resource
        HttpHeaders responseHeaders = new HttpHeaders();
        URI newGameUri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(entityId)
                .toUri();
        responseHeaders.setLocation(newGameUri);

        return new ResponseEntity<>(null, responseHeaders, HttpStatus.CREATED);

    }

}
