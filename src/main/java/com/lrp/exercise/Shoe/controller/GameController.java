package com.lrp.exercise.Shoe.controller;

import com.lrp.exercise.Shoe.model.domain.Game;
import com.lrp.exercise.Shoe.services.GameService;
import com.lrp.exercise.Shoe.services.PlayerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import java.net.URI;

import static com.lrp.exercise.Shoe.controller.BaseController.BASE_API_V1;

@RestController
@AllArgsConstructor
@RequestMapping(path = BASE_API_V1)
public class GameController extends BaseController {
    private GameService gameService;
    private PlayerService playerService;

    @RequestMapping(value="/games", method= RequestMethod.POST)
    public ResponseEntity<?> createGame(@RequestBody Game game) {
        Game savedGame = gameService.createOrUpdate(game);
        return getCreatedHttpResponse(savedGame.getId());
    }

    @RequestMapping(value="/games/{id}", method= RequestMethod.GET)
    public Game getGame(@PathVariable Long id) {
        return gameService.getGame(id);
    }

    @RequestMapping(value="/games/{id}", method= RequestMethod.DELETE)
    public void deleteGame(@PathVariable Long id) {
        if(gameService.exist(id)){
            gameService.delete(id);
        }
        else {
           String message = String.format("Game with id %d not found", id);
           throw new EntityNotFoundException(message);
        }
    }

    @RequestMapping(value="/games/{id}/shuffle", method= RequestMethod.POST)
    @Transactional
    public void shuffle(@PathVariable Long id) {
        Game game = gameService.getGame(id);
        Game shuffledGame = gameService.shuffleGameDeck(game);
        gameService.createOrUpdate(shuffledGame);
    }


    @RequestMapping(value="/games/{id}/players/{name}/dealCard", method= RequestMethod.POST)
    @Transactional
    public void dealCardToPlayer(@PathVariable Long id,@PathVariable String name,@RequestParam int nbCard) {
        Game game = gameService.getGame(id);
        gameService.dealCardToPlayer(game,name,nbCard);
    }

    @RequestMapping(value="/games/{id}/addNewDeck", method= RequestMethod.POST)
    @Transactional
    public void addNewDeck(@PathVariable Long id) {
        Game game = gameService.getGame(id);
        gameService.addNewDeck(game);
    }

}
