package com.lrp.exercise.Shoe.controller;


import com.lrp.exercise.Shoe.model.domain.Deck;
import com.lrp.exercise.Shoe.services.DeckService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@AllArgsConstructor
public class DeckController extends BaseController{

    private DeckService deckService;

    @RequestMapping(value="/decks", method= RequestMethod.POST)
    public ResponseEntity<?> createDeck()
    {
        Deck deck = deckService.createDeck();
        return getCreatedHttpResponse(deck.getId());
    }

    @RequestMapping(value="/decks/{id}", method= RequestMethod.GET)
    public Deck getDeck(@PathVariable Long id)
    {
        return deckService.getDeck(id);
    }

    protected ResponseEntity<?> getCreatedHttpResponse(Long entityId) {
        // Set the location header for the newly created resource
        HttpHeaders responseHeaders = new HttpHeaders();
        URI newGameUri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(entityId)
                .toUri();
        responseHeaders.setLocation(newGameUri);

        return new ResponseEntity<>(null, responseHeaders, HttpStatus.CREATED);

    }
}
