package com.lrp.exercise.Shoe.controller;

import com.lrp.exercise.Shoe.model.domain.Game;
import com.lrp.exercise.Shoe.model.domain.Player;
import com.lrp.exercise.Shoe.services.GameService;
import com.lrp.exercise.Shoe.services.PlayerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import static com.lrp.exercise.Shoe.controller.BaseController.BASE_API_V1;

@RestController
@RequestMapping(path= BASE_API_V1+"/games/{gameId}/players")
@AllArgsConstructor
public class PlayerController extends BaseController {

    private GameService gameService;
    private PlayerService playerService;



    @RequestMapping( method= RequestMethod.POST)
    @Transactional
    public  ResponseEntity<?> addPlayer(@PathVariable Long gameId,@RequestParam String name) {
        Game game = gameService.getGame(gameId);
        Player player = playerService.createPlayer(name);
        gameService.addPlayerToGame(game,player);
        return getCreatedHttpResponse(player.getName());
    }

    @RequestMapping(method= RequestMethod.GET)
    @Transactional
    public List<Player> getPlayers(@PathVariable Long gameId) {
        Game game = gameService.getGame(gameId);
        return new ArrayList<>(game.getPlayers());
    }

    @RequestMapping(value="/{name}", method= RequestMethod.DELETE)
    @Transactional
    public void removePlayerFromGame(@PathVariable Long gameId, @PathVariable String name) {
        Game game = gameService.getGame(gameId);
        gameService.removePlayerFromGame(game,name);
    }

}
