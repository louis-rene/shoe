package com.lrp.exercise.Shoe.model.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import org.hibernate.tuple.IdentifierAttribute;

import javax.persistence.*;
import java.util.List;

@Builder(toBuilder = true)
@Entity
@EqualsAndHashCode
@AllArgsConstructor
//Hibernate have a problem(exception)  using immutable lombok builder when updating cards
//collection.  For some reason, we have to use same instance using a set to change the card list order.
@Data
@JsonDeserialize(builder = Deck.DeckBuilder.class)
public class Deck  {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @OneToMany(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @OrderColumn
    private List<Card> cards;

    //Required by hibernate and reflection
    protected Deck() {}
}
