package com.lrp.exercise.Shoe.model.domain;

import lombok.*;

import javax.annotation.processing.Generated;
import javax.persistence.*;

@Builder
@Entity
@EqualsAndHashCode
@AllArgsConstructor
@Getter
@Embeddable
public class Card {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @EqualsAndHashCode.Exclude  //Needed so equals wont consider database ids
    private Long id;

    @Basic
    private Suit suit;

    @Basic
    private FaceValueName faceValue;


    public enum Suit {
        HEART,
        SPADE,
        CLUB,
        DIAMOND
    }

    public enum FaceValueName {
        ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN,
        JACK, QUEEN, KING
    }

    //Required for reflection and hibernate
    protected Card () { }


}
