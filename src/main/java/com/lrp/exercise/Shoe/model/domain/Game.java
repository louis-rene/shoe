package com.lrp.exercise.Shoe.model.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.annotation.processing.Generated;
import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder(toBuilder = true)
@Entity
@AllArgsConstructor
@JsonDeserialize(builder = Game.GameBuilder.class)
@EqualsAndHashCode
public class Game {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    public Long id;

    @OneToOne
    private Deck gameDeck;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @OrderBy("name")
    @Builder.Default
    private Set<Player> players = new HashSet<>();

    //Required by hibernate and reflection
    protected Game () {}
}
