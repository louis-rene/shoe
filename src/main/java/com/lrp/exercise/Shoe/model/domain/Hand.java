package com.lrp.exercise.Shoe.model.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import org.hibernate.annotations.Generated;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Data
@Builder
@Entity
@AllArgsConstructor
@JsonDeserialize(builder = Hand.HandBuilder.class)
public class Hand {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int Id;

    @OneToMany(cascade = CascadeType.ALL)
    @Builder.Default
    private List<Card> cards = new ArrayList<>();

    //Required by hibernate and reflection
    protected Hand () {}
}
