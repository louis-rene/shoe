package com.lrp.exercise.Shoe.model.dto;

import com.lrp.exercise.Shoe.model.domain.Player;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PlayerList {

    private List<PlayerRank> playerList;

    public class PlayerRank{
        private String name;
        private int Rank;
    }
}
