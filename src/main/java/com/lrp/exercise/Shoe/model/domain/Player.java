package com.lrp.exercise.Shoe.model.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.*;

import javax.persistence.*;

@Builder
@Entity
@Getter
@AllArgsConstructor
@JsonDeserialize(builder = Player.PlayerBuilder.class)
@EqualsAndHashCode
public class Player {
    @Id
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    private Hand hand;

    //Required by hibernate and reflection
    protected  Player() {}
}
