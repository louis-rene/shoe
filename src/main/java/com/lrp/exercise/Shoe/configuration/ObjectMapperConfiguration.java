package com.lrp.exercise.Shoe.configuration;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
public class ObjectMapperConfiguration {
    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();

        //This is to avoid to have to add a  @JsonPOJOBuilder(withPrefix = "") to make jackson and lombok
        //work together when using immutable builders
        //see https://www.thecuriousdev.org/lombok-builder-with-jackson/
        //This jackson configuration add the annotation if its not present at runtime
        builder.annotationIntrospector(new JacksonAnnotationIntrospector() {
                                           @Override
                                           public JsonPOJOBuilder.Value findPOJOBuilderConfig(AnnotatedClass ac) {
                                               if (ac.hasAnnotation(JsonPOJOBuilder.class)) {//If no annotation present use default as empty prefix
                                                   return super.findPOJOBuilderConfig(ac);
                                               }
                                               //make jacson call builder propreties witht he builder withouth the default "with" prefix
                                               return new JsonPOJOBuilder.Value("build", "");
                                           }
                                       }
                                       );
         return builder;
    }

}
