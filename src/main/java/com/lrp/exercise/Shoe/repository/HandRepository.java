package com.lrp.exercise.Shoe.repository;

import com.lrp.exercise.Shoe.model.domain.Hand;
import org.springframework.data.repository.CrudRepository;

public interface HandRepository extends CrudRepository<Hand,Long> {
}
