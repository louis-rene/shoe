package com.lrp.exercise.Shoe.repository;

import com.lrp.exercise.Shoe.model.domain.Deck;
import org.springframework.data.repository.CrudRepository;

public interface DeckRepository extends CrudRepository<Deck,Long> {

}
