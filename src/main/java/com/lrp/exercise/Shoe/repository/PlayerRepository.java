package com.lrp.exercise.Shoe.repository;

import com.lrp.exercise.Shoe.model.domain.Player;
import org.springframework.data.repository.CrudRepository;

public interface PlayerRepository extends CrudRepository<Player,Long> {

}
