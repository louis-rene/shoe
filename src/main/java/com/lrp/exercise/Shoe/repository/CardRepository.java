package com.lrp.exercise.Shoe.repository;

import com.lrp.exercise.Shoe.model.domain.Card;
import org.springframework.data.repository.CrudRepository;

public interface CardRepository extends CrudRepository<Card,Long> {
}
