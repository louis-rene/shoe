package com.lrp.exercise.Shoe.repository;

import com.lrp.exercise.Shoe.model.domain.Game;
import org.springframework.data.repository.CrudRepository;

public interface GameRepository extends CrudRepository<Game,Long> {

}
