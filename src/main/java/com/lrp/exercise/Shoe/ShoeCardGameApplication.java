package com.lrp.exercise.Shoe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoeCardGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoeCardGameApplication.class, args);
	}

}
