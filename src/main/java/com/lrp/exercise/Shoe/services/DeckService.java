package com.lrp.exercise.Shoe.services;

import com.lrp.exercise.Shoe.model.domain.Card;
import com.lrp.exercise.Shoe.model.domain.Deck;
import com.lrp.exercise.Shoe.model.domain.Hand;
import com.lrp.exercise.Shoe.repository.CardRepository;
import com.lrp.exercise.Shoe.repository.DeckRepository;
import com.lrp.exercise.Shoe.repository.HandRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class DeckService {

    private DeckRepository deckRepository;
    private CardRepository cardRepository;
    private HandRepository handRepository;

    public Deck createDeck()
    {
        List<Card> cards = createNewDeckCards();
        Deck newDeck = Deck.builder().cards(saveCards(cards)).build();
        return deckRepository.save(newDeck);
    }

    public void addNewDeckCards(Deck deck)
    {
        List<Card> cards = saveCards(createNewDeckCards());
        deck.getCards().addAll(cards);
        deckRepository.save(deck);
    }

    public Deck getDeck(Long id) {
        Optional<Deck> result = deckRepository.findById(id);
        if(result.isEmpty()){
            String msg = String.format("Unable to find deck id %d", id);
            throw new EntityNotFoundException(msg);
        }
        return result.get();
    }

    @Transactional
    public void dealCards(Deck deck, Hand hand, int NbCards) {
        List<Card> deckCards = deck.getCards();
        boolean changed = false;
        for(int i = 0; i < NbCards && !deckCards.isEmpty(); i++) {
                Card card = deck.getCards().remove(0);
                hand.getCards().add(card);
                changed = true;
        }
        if(changed) {
            deck.setCards(deckCards);
            deckRepository.save(deck);
            handRepository.save(hand);
        }
    }

    public Deck shuffle(Deck inputDeck)
    {
        List<Card> inputCardsCopy = new ArrayList<>(inputDeck.getCards());
        Random random = new Random();
        int listSize = inputCardsCopy.size();
        for(int i = 0; i < listSize; i++) {
            Collections.swap(inputCardsCopy,i,i + random.nextInt(listSize - i));
        }
        inputDeck.setCards(inputCardsCopy);
        return deckRepository.save(inputDeck);
    }

    public static List<Card> createNewDeckCards() {
        List<Card> newCards = new ArrayList<>();

        for (Card.Suit suit : Card.Suit.values())
        {
            for(Card.FaceValueName faceValue:Card.FaceValueName.values())
            {
                newCards.add(Card.builder().suit(suit).faceValue(faceValue).build());
            }
        }
        return newCards;
    }

    private List<Card> saveCards(List<Card> cards) {
        Iterable<Card> savedCardsIterator = cardRepository.saveAll(cards);
        List<Card> savedCards = StreamSupport.stream(savedCardsIterator.spliterator(), false)
            .collect(Collectors.toList());
        return savedCards;
    }





}
