package com.lrp.exercise.Shoe.services;

import com.lrp.exercise.Shoe.model.domain.Deck;
import com.lrp.exercise.Shoe.model.domain.Game;
import com.lrp.exercise.Shoe.model.domain.Player;
import com.lrp.exercise.Shoe.repository.GameRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GameService {

    GameRepository gameRepository;
    DeckService deckService;
    @Transactional
    public Game createOrUpdate(Game game) {

        if(game.getGameDeck() == null)
        {
            game.setGameDeck(deckService.createDeck());
        }

        return gameRepository.save(game);
    }

    public void dealCardToPlayer(Game game, String name, int nbCard) {
        for (Player p : game.getPlayers()) {
            if (p.getName().equals(name)) {
                deckService.dealCards(game.getGameDeck(), p.getHand(),nbCard);
            }
        };
        gameRepository.save(game);
    }


    public Game shuffleGameDeck(Game inputGame) {
        return inputGame.toBuilder()
                .gameDeck(deckService.shuffle(inputGame.getGameDeck()))
                .build();
    }

    public Game getGame(Long id) {
        Optional<Game> optional = gameRepository.findById(id);
        if(optional.isEmpty())
        {
            throw new EntityNotFoundException();
        }

        return optional.get();
    }
    @Transactional
    public Game addPlayerToGame(Game game, Player player)
    {
        game.getPlayers().add(player);
        return createOrUpdate(game);
    }


    /**
     * @param id the id of the game to delete
     * @return true if the game exist or false if the id is not found
     */
    public boolean exist(Long id) {

        return gameRepository.existsById(id);
    }

    /**
     * Delete the game with the specified id
     * @param id the id of the game to delete
     *
     */
    public void delete(Long id) {
        gameRepository.deleteById(id);
    }

    @Transactional
    public void addNewDeck(Game game) {

        if(game.getGameDeck() == null) {
            game.setGameDeck(deckService.createDeck());
        }
        else {
            Deck gameDeck = game.getGameDeck();
            deckService.addNewDeckCards(gameDeck);
        }
        gameRepository.save(game);
    }

    @Transactional
    public void removePlayerFromGame(Game game, String name) {
        Set newSet = game.getPlayers().stream().filter(player -> !player.getName().equals(name)).collect(Collectors.toSet());
        game.setPlayers(newSet);
        gameRepository.save(game);
    }
}
