package com.lrp.exercise.Shoe.services;

import com.lrp.exercise.Shoe.model.domain.Hand;
import com.lrp.exercise.Shoe.model.domain.Player;
import com.lrp.exercise.Shoe.repository.HandRepository;
import com.lrp.exercise.Shoe.repository.PlayerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class PlayerService {

    PlayerRepository playerRepository;

    @Transactional
    public Player createPlayer(String playerName) {
        Hand hand = Hand.builder().build();
        Player player = Player.builder().name(playerName).hand(hand).build();
        return playerRepository.save(player);
    }
}
