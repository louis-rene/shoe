package com.lrp.exercise.Shoe.repository;

import com.lrp.exercise.Shoe.model.domain.Card;
import com.lrp.exercise.Shoe.model.domain.Game;
import com.lrp.exercise.Shoe.testUtility.TestCardFactory;
import com.lrp.exercise.Shoe.testUtility.TestGameFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@DisplayName("Given a configured game repository")
class GameRepositoryTest {

    @Autowired
    private GameRepository repository;

    @DisplayName(" when saving a game")
    @Nested
    class WhenSavingAGame {

        private Game gameToSave;
        private Game savedGame;

        @BeforeEach
        void saveGame() {
            gameToSave = TestGameFactory.createEmptyGame();
            savedGame = repository.save(gameToSave);
        }

        @Test
        @DisplayName("then saved game is equald to the game to save")
        public void savedGameEquals()
        {
           assertThat(savedGame).isEqualTo(gameToSave);
        }
    }

    @Nested
    @DisplayName(" when multiple game are saved")
    class  WhenMultipleCardAreSaved {

        private Game savedGame1;
        private Game savedGame2;
        private Game gameToSave1;
        private Game gameToSave2;


        @BeforeEach
        public void saveCards() {
            gameToSave1 = TestGameFactory.createEmptyGame();
            gameToSave2 = TestGameFactory.createEmptyGame();
            savedGame1 = repository.save(gameToSave1);
            savedGame2 = repository.save(gameToSave2);
        }

        @DisplayName(" then the generated id of the game is not the same")
        @Test
        public void idIsNotSame()
        {
            assertThat(savedGame1.getId()).isNotEqualTo(savedGame2.getId());
        }
    }


}