package com.lrp.exercise.Shoe.repository;

import com.lrp.exercise.Shoe.model.domain.Card;
import com.lrp.exercise.Shoe.testUtility.TestCardFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@DisplayName("Given a configured card repository")
class CardRepositoryTest {

    @Autowired
    CardRepository repository;

    @Nested
    @DisplayName(" when card is saved")
    class  WhenCardIsSaved {

        private Card savedCard;
        private Card cardToSave;

        @BeforeEach
        public void saveCard()
        {
            cardToSave = TestCardFactory.createAceOfSpade();
            savedCard = repository.save(cardToSave);
        }

        @Test
        @DisplayName(" then saved card is equals to card to save")
        public void cardIsSaved()
        {
            assertThat(savedCard).isEqualTo(cardToSave);
        }

        @Test
        @DisplayName(" the card id is filled by hibernate")
        public void idNotNull()
        {
            assertThat(savedCard.getId()).isNotNull();
        }
    }

    @Nested
    @DisplayName(" when multiple card are saved")
    class  WhenMultipleCardAreSaved {

        private Card savedCard1;
        private Card savedCard2;
        private Card cardToSave1;
        private Card cardToSave2;


        @BeforeEach
        public void saveCards() {
            cardToSave1 = TestCardFactory.createCard(Card.Suit.HEART, Card.FaceValueName.JACK);
            cardToSave2 = TestCardFactory.createCard(Card.Suit.DIAMOND, Card.FaceValueName.NINE);
            savedCard1 = repository.save(cardToSave1);
            savedCard2 = repository.save(cardToSave2);
        }

        @DisplayName(" then the generated id of the cards is not the same")
        @Test
        public void idIsNotSame()
        {
            assertThat(savedCard1.getId()).isNotEqualTo(savedCard2.getId());

        }
    }


}