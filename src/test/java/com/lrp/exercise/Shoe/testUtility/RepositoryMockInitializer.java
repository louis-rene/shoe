package com.lrp.exercise.Shoe.testUtility;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.data.repository.CrudRepository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class RepositoryMockInitializer<T,D> {


    public static <T,D> void  repoSaveAllWillReturnInputEntity(CrudRepository<T,D> repository) {
        //crudRepo return the saved object filled with id in the save function
        //So for this test we assume all when well and return the input list passed in argument
        when(repository.saveAll(any(Iterable.class))).thenAnswer(new Answer<Iterable>() {
            @Override
            public Iterable answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                return (Iterable) args[0];
            }
        });

    }

    public static <T,D> void  repoSaveWillReturnInputEntity(CrudRepository<T,D> repository,Class<T> tClass) {

        //crudRepo return the saved object filled with id in the save function
        //So for this test we assume all when well and return the input list passed in argument
        when(repository.save(any(tClass))).thenAnswer(new Answer<T>() {
            @Override
            public T answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                return (T) args[0];
            }
        });
    }
}
