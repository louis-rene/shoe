package com.lrp.exercise.Shoe.testUtility;


import com.lrp.exercise.Shoe.model.domain.Card;

public class TestCardFactory {

    public static Card createAceOfSpade()
    {
        return Card.builder().faceValue(Card.FaceValueName.ACE).suit(Card.Suit.CLUB).build();
    }

    public static Card createCard(Card.Suit suit,Card.FaceValueName faceValueName)
    {
        return Card.builder().suit(suit).faceValue(faceValueName).build();
    }
}
