package com.lrp.exercise.Shoe.testUtility;

import com.lrp.exercise.Shoe.model.domain.Game;
import com.lrp.exercise.Shoe.model.domain.Hand;
import com.lrp.exercise.Shoe.model.domain.Player;

import java.util.HashSet;

public class TestGameFactory {

    public static Game createEmptyGame() {
        return Game.builder().build();
    }

    public static Game createGameWithPlayer() {
        Player player = Player.builder().name("bob").hand(Hand.builder().build()).build();
        HashSet<Player> playerSet = new HashSet<>();
        playerSet.add(player);
        Game gameWithCards = Game.builder()
                .players(playerSet)
                .gameDeck(TestDeckFactory.createNewDeck()).build();
        return gameWithCards;
    }
}
