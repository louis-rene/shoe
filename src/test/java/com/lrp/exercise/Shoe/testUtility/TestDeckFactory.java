package com.lrp.exercise.Shoe.testUtility;

import com.lrp.exercise.Shoe.model.domain.Card;
import com.lrp.exercise.Shoe.model.domain.Deck;
import com.lrp.exercise.Shoe.services.DeckService;

import java.util.List;

public class TestDeckFactory {
    public static Deck createNewDeck() {
        return Deck.builder().cards(TestDeckFactory.createNewDeckCards()).build();
    }
    public static List<Card> createNewDeckCards() { return DeckService.createNewDeckCards();}
}
