package com.lrp.exercise.Shoe.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lrp.exercise.Shoe.configuration.ObjectMapperConfiguration;
import com.lrp.exercise.Shoe.model.domain.Game;
import com.lrp.exercise.Shoe.model.domain.Player;
import com.lrp.exercise.Shoe.services.GameService;
import com.lrp.exercise.Shoe.services.PlayerService;
import com.lrp.exercise.Shoe.testUtility.TestGameFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {PlayerController.class,ObjectMapperConfiguration.class})
@DisplayName("Given a configured PlayerController")
public class PlayerControllerTest {

    public static final long GAME_ID = 42l;

    @Autowired
    private ObjectMapper jacksonMapper;

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private GameService gameService;

    @MockBean
    private PlayerService playerService;

    @Nested
    @DisplayName(" when getting player data")
    class WhenGettingPlayerData {
        private Game createdGame;
        private MockHttpServletRequestBuilder playerListRequestBuilder;
        private MockHttpServletRequestBuilder requestBuilder;


        @BeforeEach
        public void setupMock() throws JsonProcessingException {
           createdGame = TestGameFactory.createGameWithPlayer().toBuilder().id(GAME_ID).build();
           when(gameService.getGame(createdGame.getId())).thenReturn(createdGame);

           String url = String.format("/api/v1/games/%d/players",createdGame.getId());
            requestBuilder = MockMvcRequestBuilders.get(url)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON);
        }

        @Test
        @DisplayName("then game player list returned")
        public void thenGamePlayerListIsReturned() throws Exception {
            MvcResult result = mockMvc.perform(requestBuilder)
                    .andExpect(status().isOk())
                    .andReturn();

            String json = result.getResponse().getContentAsString();
            Player[] listOfPlayer = jacksonMapper.readValue( json,Player[].class);

            Player[] expected = createdGame.getPlayers().toArray(new Player[listOfPlayer.length]);
            assertThat(Arrays.asList(listOfPlayer)).containsExactlyInAnyOrder(expected);
        }

    }

    @Nested
    @DisplayName(" when adding a player to a game player data")
    class WhenAddingPlayerToAGame {
        public static final String PLAYER_NAME = "bob";
        private Game createdGame;
        private MockHttpServletRequestBuilder playerListRequestBuilder;
        private MockHttpServletRequestBuilder requestBuilder;
        private Player createdPlayer;

        @BeforeEach
        public void setupMock() throws JsonProcessingException {
            createdGame = TestGameFactory.createEmptyGame().toBuilder().id(GAME_ID).build();
            createdPlayer = Player.builder().name(PLAYER_NAME).build();

            when(gameService.getGame(createdGame.getId())).thenReturn(createdGame);
            when(playerService.createPlayer(PLAYER_NAME)).thenReturn(createdPlayer);
            String url = getBaseUrl(GAME_ID);
            requestBuilder = MockMvcRequestBuilders.post(url).param("name",PLAYER_NAME)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON);
        }

        @Test
        @DisplayName("then a player is created with name in parameter list returned")
        public void thenGamePlayerListIsReturned() throws Exception {
           mockMvc.perform(requestBuilder)
                    .andExpect(status().isCreated())
                    .andExpect(header().exists(HttpHeaders.LOCATION))
                    .andExpect(header().string(HttpHeaders.LOCATION, "http://localhost"+ getBaseUrl(GAME_ID,PLAYER_NAME)));

            verify(playerService).createPlayer(PLAYER_NAME);
        }

    }

    @Nested
    @DisplayName(" when deleting a player from a game")
    class WhenDeletingPlayerToAGame {
        public static final String PLAYER_NAME = "bob";
        private Game createdGame;
        private MockHttpServletRequestBuilder playerListRequestBuilder;
        private MockHttpServletRequestBuilder requestBuilder;
        private Player createdPlayer;


        @BeforeEach
        public void setupMock() throws JsonProcessingException {
            createdPlayer = Player.builder().name(PLAYER_NAME).build();
            createdGame = TestGameFactory.createGameWithPlayer();

            when(gameService.getGame(GAME_ID)).thenReturn(createdGame);

            requestBuilder = MockMvcRequestBuilders.delete(getBaseUrl(GAME_ID,PLAYER_NAME))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON);
        }

        @Test
        @DisplayName("then a game service is called to remove that player from the game")
        public void thenGamePlayerListIsReturned() throws Exception {
            mockMvc.perform(requestBuilder)
                    .andExpect(status().isOk());

            verify(gameService).removePlayerFromGame(createdGame,PLAYER_NAME);
        }

    }

    private String getBaseUrl(long gameID) {
        return String.format("/api/v1/games/%d/players",gameID);
    }
    private String getBaseUrl(long gameID,String playerName) {
        return String.format("/api/v1/games/%d/players/%s",gameID,playerName);
    }

}