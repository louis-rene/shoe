package com.lrp.exercise.Shoe.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lrp.exercise.Shoe.configuration.ObjectMapperConfiguration;
import com.lrp.exercise.Shoe.model.domain.Game;
import com.lrp.exercise.Shoe.services.GameService;
import com.lrp.exercise.Shoe.services.PlayerService;
import com.lrp.exercise.Shoe.testUtility.TestGameFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {GameController.class,ObjectMapperConfiguration.class})
@DisplayName("Given a configured GameController")
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper jacksonMapper;

    @MockBean
    private GameService gameService;

    @MockBean
    private PlayerService playerService;

    @Nested
    @DisplayName(" when making a post rest call to create a game")
    class WhenMakingPostRestCall {

        private Game gameToCreate;
        private Game createdGame;
        private MockHttpServletRequestBuilder emptyGamePostRequestBuilder;

        @BeforeEach
        public void setupMock() throws JsonProcessingException {
            gameToCreate = TestGameFactory.createEmptyGame();
            String json = jacksonMapper.writeValueAsString(gameToCreate);

            createdGame = TestGameFactory.createEmptyGame().toBuilder().id(42l).build();
            when(gameService.createOrUpdate(any(Game.class))).thenReturn(createdGame);

            emptyGamePostRequestBuilder = MockMvcRequestBuilders.post("/api/v1/games")
                    .content(json)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON);
        }


        @Test
        @DisplayName(" then created game url is returned in header")
        public void createGameUrlReturned() throws Exception {
            mockMvc.perform(emptyGamePostRequestBuilder)
                    .andExpect(status().isCreated())
                    .andExpect(header().exists(HttpHeaders.LOCATION))
                    .andExpect(header().string(HttpHeaders.LOCATION, "http://localhost/api/v1/games/42"));

            verify(gameService).createOrUpdate(Mockito.eq(gameToCreate));
        }
    }

    @Nested
    @DisplayName(" when deleting an existing game")
    class WhenMakingDelete {

        private MockHttpServletRequestBuilder emptyGamePostRequestBuilder;

        @BeforeEach
        private void setupMock() throws JsonProcessingException {
            emptyGamePostRequestBuilder = MockMvcRequestBuilders.delete("/api/v1/games/42")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON);
            when(gameService.exist(42l)).thenReturn(true);
        }


        @Test
        @DisplayName(" service is called and ok is returned")
        public void createGameUrlReturned() throws Exception {
            mockMvc.perform(emptyGamePostRequestBuilder)
                    .andExpect(status().isOk());

            verify(gameService).delete(Mockito.eq(42l));
        }
    }

    @Nested
    @DisplayName(" when shuffling a game deck")
    class WhenShufflingGameDeck {

        private MockHttpServletRequestBuilder emptyGamePostRequestBuilder;
        private Game mockedGame;

        @BeforeEach
        private void setupMock() throws JsonProcessingException {
            emptyGamePostRequestBuilder = MockMvcRequestBuilders.post("/api/v1/games/42/shuffle")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON);
            mockedGame = TestGameFactory.createEmptyGame();
            when(gameService.getGame(42l)).thenReturn(mockedGame);
        }


        @Test
        @DisplayName(" game service shuffle is called")
        public void createGameUrlReturned() throws Exception {
            mockMvc.perform(emptyGamePostRequestBuilder)
                    .andExpect(status().isOk());
            verify(gameService).shuffleGameDeck(eq(mockedGame));
        }
    }
}

