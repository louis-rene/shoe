package com.lrp.exercise.Shoe.services;

import com.lrp.exercise.Shoe.model.domain.Card;
import com.lrp.exercise.Shoe.model.domain.Deck;
import com.lrp.exercise.Shoe.model.domain.Game;
import com.lrp.exercise.Shoe.model.domain.Hand;
import com.lrp.exercise.Shoe.repository.CardRepository;
import com.lrp.exercise.Shoe.repository.DeckRepository;
import com.lrp.exercise.Shoe.repository.HandRepository;
import com.lrp.exercise.Shoe.testUtility.RepositoryMockInitializer;
import com.lrp.exercise.Shoe.testUtility.TestDeckFactory;
import com.lrp.exercise.Shoe.testUtility.TestGameFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@DisplayName("Given a deck service ")
class DeckServiceTest {
    @Mock
    private DeckRepository deckRepository;

    @Mock
    private HandRepository handRepository;

    @Mock
    private CardRepository cardRepository;

    @InjectMocks
    private DeckService service;


    @Nested
    @DisplayName(" when creating a deck")
    class WhenCreatingADeck {

        private Deck deckCreated;

        @BeforeEach
        public void setupMock() {
            RepositoryMockInitializer.repoSaveAllWillReturnInputEntity(cardRepository);
            RepositoryMockInitializer.repoSaveWillReturnInputEntity(deckRepository,Deck.class);
            //invoke
            deckCreated = service.createDeck();
        }

        @Test
        @DisplayName("then 52 card are created.")
        public void fiftyTwoCardCreated(){
            assertThat(deckCreated.getCards().size()).isEqualTo(52);
        }

        @Test
        @DisplayName("card created in order.")
        public void cardCreatedInOrder() {
            Card first = Card.builder().suit(Card.Suit.HEART).faceValue(Card.FaceValueName.ACE).build();
            Card last = Card.builder().suit(Card.Suit.DIAMOND).faceValue(Card.FaceValueName.KING).build();
            assertThat(deckCreated.getCards().lastIndexOf(first)).isEqualTo(0);
            assertThat(deckCreated.getCards().lastIndexOf(last)).isEqualTo(51);
        }

    }

    @Nested
    @DisplayName(" when shuffleing a deck")
    class WhenShufflelingADeck {

        private Deck deckToShuffle;
        private List<Card> previousOrder;
        private Deck shuffledDeck;
        @BeforeEach
        public void setupDeck() {
            RepositoryMockInitializer.repoSaveAllWillReturnInputEntity(cardRepository);
            RepositoryMockInitializer.repoSaveWillReturnInputEntity(deckRepository,Deck.class);
            deckToShuffle = service.createDeck();
            previousOrder = new ArrayList<Card>(deckToShuffle.getCards());
            shuffledDeck = service.shuffle(deckToShuffle);
        }

        @Test
        @DisplayName("then the card are the sames")
        public void same() {
            assertThat(shuffledDeck.getCards()).containsExactlyInAnyOrder(deckToShuffle.getCards().toArray(new Card[deckToShuffle.getCards().size()]));
        }

        @Test
        @DisplayName("then the card not in same order")
        public void randomizedCards() {
            assertThat(shuffledDeck.getCards()).doesNotContainSequence(previousOrder.toArray(new Card[deckToShuffle.getCards().size()]));
        }

    }

    @Nested
    @DisplayName(" when dealing cards to a player")
    class WhenDealingCardsToAPlayer {
        private static final int NB_CARD = 3;
        private Deck deck;
        private List<Card> previousOrder;
        private Hand playerHand;
        @BeforeEach
        public void setupDeck() {
            Game game = TestGameFactory.createGameWithPlayer();
            playerHand = game.getPlayers().iterator().next().getHand();
            deck = game.getGameDeck();
            service.dealCards(game.getGameDeck(),playerHand,NB_CARD);
        }

        @Test
        @DisplayName("player hand should have the number of card draw")
        public void nbCardInHand() {
            assertThat(playerHand.getCards().size()).isEqualTo(NB_CARD);
        }

        @Test
        @DisplayName("Deck should have less card")
        public void randomizedCards() {
            assertThat(deck.getCards().size()).isEqualTo(52-NB_CARD);
        }

        @Test
        @DisplayName("hand repository save should had been called")
        public void handRepoCalled() {
            verify(handRepository).save(playerHand);
        }

        @Test
        @DisplayName("deck repository save should had been called")
        public void deckRepoCalled() {
            verify(deckRepository).save(deck);
        }
    }



}