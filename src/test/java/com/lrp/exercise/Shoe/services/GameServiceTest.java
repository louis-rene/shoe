package com.lrp.exercise.Shoe.services;

import com.lrp.exercise.Shoe.model.domain.Game;
import com.lrp.exercise.Shoe.model.domain.Player;
import com.lrp.exercise.Shoe.repository.GameRepository;
import com.lrp.exercise.Shoe.testUtility.RepositoryMockInitializer;
import com.lrp.exercise.Shoe.testUtility.TestGameFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Given a game service ")
class GameServiceTest {

    @Mock
    private GameRepository gameRepository;

    @Mock
    private DeckService deckService;


    @InjectMocks
    private GameService gameService;

    @DisplayName(" when creating a game")
    @Nested
    class WhenCreatingAGame {

        @Test
        @DisplayName(" then repository called")
        public void thenRepositoryCalled()
        {
            Game game = TestGameFactory.createEmptyGame();

            gameService.createOrUpdate(game);

            verify(gameRepository).save(Mockito.eq(game));
        }

        @Test
        @DisplayName(" then deck created when null")
        public void thenDeckCreatedWhenNull()
        {
            Game game = TestGameFactory.createEmptyGame().toBuilder().gameDeck(null).build();

            gameService.createOrUpdate(game);

            verify(deckService,atLeastOnce()).createDeck();
        }
    }

    @DisplayName(" when deleting a game")
    @Nested
    class WhenDeletingAGame {

        public static final long ID = 42L;

        @Test
        @DisplayName(" then repository called")
        public void thenRepositoryCalled()
        {
            Game game = TestGameFactory.createEmptyGame();

            gameService.delete(ID);

            verify(gameRepository).deleteById(ID);
        }
    }


    @DisplayName(" when adding a player to a game")
    @Nested
    class AddingPlayerToGame {

        public static final long ID = 42L;

        @BeforeEach
        public void initMock()
        {
            RepositoryMockInitializer.repoSaveWillReturnInputEntity(gameRepository,Game.class);
        }

        @Test
        @DisplayName(" then game contain player")
        public void thenRepositoryCalled()
        {
            Game game = TestGameFactory.createEmptyGame();
            Player player = Player.builder().name("bob").build();
            Game result = gameService.addPlayerToGame(game,player);

            assertThat(result.getPlayers()).contains(player);
        }
    }

    @DisplayName(" when dealing card to a player")
    @Nested
    class DealingCardsToPlayer {

        public static final long ID = 42L;
        private static final int NB_CARD = 3;

        private Game gameWithCards;
        private Player player;
        @BeforeEach
        public void initMock() {
            RepositoryMockInitializer.repoSaveWillReturnInputEntity(gameRepository, Game.class);
            gameWithCards = TestGameFactory.createGameWithPlayer();
            player = gameWithCards.getPlayers().iterator().next();
            gameService.dealCardToPlayer(gameWithCards,player.getName(),NB_CARD);
        }

        @Test
        @DisplayName(" then game service will call deck service for dealing card to player")
        public void thenRepositoryCalled()
        {
           verify(deckService, times(1)).dealCards(gameWithCards.getGameDeck(),player.getHand(),NB_CARD);

        }

        @Test
        @DisplayName(" then game service will call save on game repository with the game")
        public void thenSaveCalled()
        {
            verify(gameRepository, times(1)).save(gameWithCards);

        }
    }

}
