package com.lrp.exercise.Shoe.services;

import com.lrp.exercise.Shoe.model.domain.Player;
import com.lrp.exercise.Shoe.repository.PlayerRepository;
import com.lrp.exercise.Shoe.testUtility.RepositoryMockInitializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@DisplayName("Given a player service ")
class PlayerServiceTest {

    @Mock
    private PlayerRepository playerRepository;




    @InjectMocks
    private PlayerService playerService;

    @Nested
    @DisplayName("when creating a player")
    class WhenCreatingPlayer {

        public final static String NEW_PLAYER_NAME = "bob";
        @BeforeEach
        private void initMock()
        {
            RepositoryMockInitializer.repoSaveWillReturnInputEntity(playerRepository, Player.class);
        }

        @Test
        @DisplayName("then player repository called with new player")
        public void playerRepositoryCalled() {
            Player result = playerService.createPlayer(NEW_PLAYER_NAME);
            assertThat(result.getName()).isEqualTo(NEW_PLAYER_NAME);
        }


    }

}